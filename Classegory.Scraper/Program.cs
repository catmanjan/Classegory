﻿using Classegory.Models;
using Nest;
using OpenScraping;
using OpenScraping.Config;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Classegory.Scraper
{
    class Program
    {
        static ConfigSection config;
        static ConnectionSettings settings;
        static ElasticClient elastic;

        static void Main(string[] args)
        {
            var configJson = @"
            {
                'category': '//*[@id=""divClaSubMenu""]/div[1]/div/h2',
                'content': '//*[@id=""fullDescription""]/dl/dd'
            }
            ";

            config = StructuredDataConfig.ParseJsonString(configJson);

            long start = 71726;

            var uri = new Uri("http://elastic:changeme@localhost:9200/");

            settings = new ConnectionSettings(uri)
                .DefaultIndex("classegory")
                .InferMappingFor<Classified>(i => i
                    .TypeName("classified")
                    .IndexName("classegory")
                )
                .DisableDirectStreaming(true);

            elastic = new ElasticClient(settings);

            for (long q = start; q > 0; q -= 10000)
            {
                var urls = new List<string>();

                for (long i = start; i > start - 10000; i--)
                {
                    urls.Add("https://www.allbids.com.au/ads/" + i);
                }

                Parallel.ForEach(urls, new ParallelOptions { MaxDegreeOfParallelism = 100 }, url => { Download(url); });
            }

            Console.ReadLine();
        }

        static void Download(string url)
        {
            try
            {
                var html = "";

                using (var client = new WebClient())
                {
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    html = client.DownloadString(url);
                }

                var openScraping = new StructuredDataExtractor(config);
                var scrapingResults = openScraping.Extract(html);

                Console.WriteLine(scrapingResults["category"] + " -> " + scrapingResults["content"]);

                if (scrapingResults["category"] != null &&
                    scrapingResults["content"] != null)
                {
                    var classified = new Classified()
                    {
                        Category = scrapingResults["category"].ToString(),
                        Content = scrapingResults["content"].ToString()
                    };

                    elastic.Index(classified);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Skip...");
            }
        }
    }
}
