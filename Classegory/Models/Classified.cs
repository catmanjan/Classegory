﻿using Nest;

namespace Classegory.Models
{
    public class Classified
    {
        /// <summary>
        /// Category of the ad, e.g. "Car"
        /// </summary>
        [Text(Name = "category", Index = true, TermVector = TermVectorOption.Yes)]
        public string Category { get; set; }
        /// <summary>
        /// Content of the ad, e.g. "2005 Toyota Corolla long rego no problems"
        /// </summary>
        [Text(Name = "content", Index = true, TermVector = TermVectorOption.Yes)]
        public string Content { get; set; }
    }
}