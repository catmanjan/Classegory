﻿using System;
using System.Linq;
using System.Web.Mvc;
using Classegory.Models;
using Nest;

namespace Classegory.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Return the category recommended based on text provided by the user.
        /// 
        /// For example, as the user fills in their classified ad, the category recommendation could autopopulate for a more seamless experience.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public JsonResult Category(string text)
        {
            var uri = new Uri("http://elastic:changeme@localhost:9200/");
            var settings = new ConnectionSettings(uri);
            var client = new ElasticClient(settings);

            var response = client.Search<Classified>(search => search
                .Index("classegory")
                .Query(query => query.MoreLikeThis(moreLikeThis => moreLikeThis
                    .Fields(fields => fields
                        .Field(field => field.Content))
                    .Like(like => like.Text(text))
                    .MinTermFrequency(1)
                    .MinDocumentFrequency(1))));

            var result = response.Hits.Select(hit => new
            {
                Category = hit.Source.Category,
                Score = hit.Score
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}